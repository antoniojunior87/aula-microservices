package br.uniriotec.folhapagamento.service.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe que mapeia os campos retornados pelo servico de Frequencia dos
 * Funcionarios para conversao automatica na deserializacao.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PontoFuncionario {

	private Long cpfFuncionario;
	private Integer competencia;
	private Integer diaTrabalhados;

	public PontoFuncionario() {
	}

	public Long getCpfFuncionario() {
		return cpfFuncionario;
	}

	public Integer getCompetencia() {
		return competencia;
	}

	public Integer getDiaTrabalhados() {
		return diaTrabalhados;
	}

	public void setCpfFuncionario(Long cpfFuncionario) {
		this.cpfFuncionario = cpfFuncionario;
	}

	public void setCompetencia(Integer competencia) {
		this.competencia = competencia;
	}

	public void setDiaTrabalhados(Integer diaTrabalhados) {
		this.diaTrabalhados = diaTrabalhados;
	}
}