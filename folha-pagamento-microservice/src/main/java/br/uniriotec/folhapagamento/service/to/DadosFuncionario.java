package br.uniriotec.folhapagamento.service.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe que mapeia os campos retornados pelo servico de Funcionarios para
 * conversao automatica na deserializacao.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DadosFuncionario {

	private Long cpf;
	private Integer diasTrabalhoMes;
	private Double salarioBase;

	public DadosFuncionario() {

	}

	public Long getCpf() {
		return cpf;
	}

	public Double getSalarioBase() {
		return salarioBase;
	}

	public Integer getDiasTrabalhoMes() {
		return diasTrabalhoMes;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public void setSalarioBase(Double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public void setDiasTrabalhoMes(Integer diasTrabalhoMes) {
		this.diasTrabalhoMes = diasTrabalhoMes;
	}
}