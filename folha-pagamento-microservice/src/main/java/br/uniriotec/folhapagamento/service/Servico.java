package br.uniriotec.folhapagamento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.uniriotec.folhapagamento.service.to.Remuneracao;

@RestController
public class Servico {

	@Autowired
	private FolhaPagamentoService service;

	// declara metodos REST
	@CrossOrigin
	@RequestMapping(path = "/folhapagamento/{competencia}/{cpf}", method = RequestMethod.GET)
	public Remuneracao obterRemuneracaoPorCpfCompetencia(@PathVariable Long cpf, @PathVariable Integer competencia) {
		return service.remuneracaoFuncionarioPorCompetencia(cpf, competencia);
	}

	@CrossOrigin
	@RequestMapping(path = "/folhapagamento/{competencia}", method = RequestMethod.GET)
	public List<Remuneracao> obterFolhaPagamentoPorCompetencia(@PathVariable Integer competencia) {
		return service.folhaPagamentoPorCompetencia(competencia);
	}

}
