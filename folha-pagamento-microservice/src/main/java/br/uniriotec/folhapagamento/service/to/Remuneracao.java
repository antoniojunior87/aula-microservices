package br.uniriotec.folhapagamento.service.to;

import java.io.Serializable;

/**
 * Classe que mapeia os campos retornados
 *
 */
public class Remuneracao implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long cpf;

	private Double salario;

	private Integer competencia;

	public Remuneracao() {

	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Integer getCompetencia() {
		return competencia;
	}

	public void setCompetencia(Integer competencia) {
		this.competencia = competencia;
	}
}
