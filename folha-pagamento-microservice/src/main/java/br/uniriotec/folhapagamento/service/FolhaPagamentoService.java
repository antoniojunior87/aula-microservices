package br.uniriotec.folhapagamento.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.uniriotec.folhapagamento.service.to.DadosFuncionario;
import br.uniriotec.folhapagamento.service.to.PontoFuncionario;
import br.uniriotec.folhapagamento.service.to.Remuneracao;

@Service
public class FolhaPagamentoService {

	// o nome do host eh o mesmo nome do container 
	private static final String PREFIXO_SERVICO_FUNCIONARIOS = "http://funcionarios:8081/";
	private static final String PREFIXO_SERVICO_FREQUENCIA = "http://frequencia-ponto:8082/";
	
	// exemplo url de chamada em ambiente local para teste
	//private static final String PREFIXO_SERVICO_FUNCIONARIOS = "http://localhost:8081/";
	//private static final String PREFIXO_SERVICO_FREQUENCIA = "http://localhost:8082/";

	public Remuneracao remuneracaoFuncionarioPorCompetencia(Long cpf, Integer competencia) {

		RestTemplate restTemplate = new RestTemplate();

		Map<String, String> vars = new HashMap<String, String>();
		vars.put("cpf", cpf.toString());
		vars.put("competencia", competencia.toString());

		// invoca microservico
		PontoFuncionario pontoFuncionario = restTemplate.getForObject(
				PREFIXO_SERVICO_FREQUENCIA + "frequenciaponto/{cpf}/{competencia}", PontoFuncionario.class, vars);
		
		if (pontoFuncionario == null) {
			return null;
		}

		vars = new HashMap<String, String>();
		vars.put("cpf", cpf.toString());

		DadosFuncionario funcionario = restTemplate.getForObject(PREFIXO_SERVICO_FUNCIONARIOS + "funcionarios/{cpf}",
				DadosFuncionario.class, vars);

		Double salarioPorDia = funcionario.getSalarioBase() / funcionario.getDiasTrabalhoMes();

		Double salarioCompetencia = salarioPorDia * pontoFuncionario.getDiaTrabalhados();

		Remuneracao remuneracao = new Remuneracao();
		remuneracao.setCompetencia(competencia);
		remuneracao.setCpf(cpf);
		remuneracao.setSalario(salarioCompetencia);

		return remuneracao;
	}

	public List<Remuneracao> folhaPagamentoPorCompetencia(Integer competencia) {

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<DadosFuncionario[]> responseEntity = restTemplate
				.getForEntity(PREFIXO_SERVICO_FUNCIONARIOS + "funcionarios", DadosFuncionario[].class);
		
		List<DadosFuncionario> funcionarios = Arrays.asList(responseEntity.getBody());
		
		if (funcionarios == null) {
			return new ArrayList<Remuneracao>();
		}

		List<Remuneracao> folhaPagamento = new ArrayList<Remuneracao>();

		for (DadosFuncionario dadosFuncionario : funcionarios) {
			Map<String, String> vars = new HashMap<String, String>();
			vars.put("cpf", dadosFuncionario.getCpf().toString());
			vars.put("competencia", competencia.toString());

			PontoFuncionario pontoFuncionario = restTemplate.getForObject(
					PREFIXO_SERVICO_FREQUENCIA + "frequenciaponto/{cpf}/{competencia}", PontoFuncionario.class, vars);
			
			if (pontoFuncionario == null) {
				continue;
			}

			Double salarioPorDia = dadosFuncionario.getSalarioBase() / dadosFuncionario.getDiasTrabalhoMes();

			Double salarioCompetencia = salarioPorDia * pontoFuncionario.getDiaTrabalhados();

			Remuneracao remuneracao = new Remuneracao();
			remuneracao.setCompetencia(competencia);
			remuneracao.setCpf(dadosFuncionario.getCpf());
			remuneracao.setSalario(salarioCompetencia);

			folhaPagamento.add(remuneracao);
		}

		return folhaPagamento;
	}

}