package br.uniriotec.funcionarios.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long cpf;

	@Column
	private String nome;

	@JsonFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dataAdmissao;

	/* qtd dias que deve trabalhar por mes, padrao 20 dias */
	private Integer diasTrabalhoMes = 20;

	@Column
	private Double salarioBase;

	public Funcionario() {
		super();
	}

	public Long getCpf() {
		return cpf;
	}

	public String getNome() {
		return nome;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public Double getSalarioBase() {
		return salarioBase;
	}

	public Integer getDiasTrabalhoMes() {
		return diasTrabalhoMes;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public void setSalarioBase(Double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public void setDiasTrabalhoMes(Integer diasTrabalhoMes) {
		this.diasTrabalhoMes = diasTrabalhoMes;
	}

}
