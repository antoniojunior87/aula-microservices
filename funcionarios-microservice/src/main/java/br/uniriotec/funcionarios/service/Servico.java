package br.uniriotec.funcionarios.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.uniriotec.funcionarios.entity.Funcionario;

@RestController
public class Servico {

	@Autowired
	private FuncionarioService service;

	@CrossOrigin
	@RequestMapping(path = "/funcionarios", method = RequestMethod.GET)
	public List<Funcionario> listarTodos() {
		return service.listarTodos();
	}

	@CrossOrigin
	@RequestMapping(path = "/funcionarios/{cpf}", method = RequestMethod.GET)
	public Funcionario obterPorCpf(@PathVariable Long cpf) {
		return service.obterPorCpf(cpf);
	}

	@PostConstruct
	@CrossOrigin
	@RequestMapping(path = "/setup", method = RequestMethod.GET)
	public String setUp() throws ParseException {

		// insere dados iniciais para teste

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		List<Funcionario> lista = new ArrayList<Funcionario>();

		Funcionario f;

		f = new Funcionario();
		f.setCpf(12345678901L);
		f.setDataAdmissao(sdf.parse("01/01/2010"));
		f.setNome("Funcionario 1");
		f.setSalarioBase(2500.00);
		lista.add(f);

		f = new Funcionario();
		f.setCpf(20030040099L);
		f.setDataAdmissao(sdf.parse("20/10/2014"));
		f.setNome("Funcionario 2");
		f.setSalarioBase(3700.00);
		lista.add(f);

		f = new Funcionario();
		f.setCpf(60070080099L);
		f.setDataAdmissao(sdf.parse("05/07/2015"));
		f.setNome("Funcionario 3");
		f.setSalarioBase(5500.00);
		lista.add(f);

		try {
			service.salvarTodos(lista);
			return "Dados para teste carregados!";
		} catch (Exception ex) {
			return "Erro - os dados ja estavao carregados?";
		}
	}
}
