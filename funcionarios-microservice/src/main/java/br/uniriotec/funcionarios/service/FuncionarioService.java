package br.uniriotec.funcionarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.uniriotec.funcionarios.dao.GenericDao;
import br.uniriotec.funcionarios.entity.Funcionario;

@Service
public class FuncionarioService {

	@Autowired
	private GenericDao dao;

	public void salvarTodos(List<Funcionario> lista) {

		for (Funcionario funcionario : lista) {
			dao.save(funcionario);
		}
	}

	public List<Funcionario> listarTodos() {
		return dao.findAll(Funcionario.class);
	}

	public Funcionario obterPorCpf(Long cpf) {
		return dao.findOne(Funcionario.class, cpf);
	}
}
