package br.uniriotec.frequenciaponto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FrequenciaFuncionario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private Long cpfFuncionario;

	/* YYYYMM - exemplo: 201611 */
	@Column
	private Integer competencia;

	private Integer diaTrabalhados;

	public FrequenciaFuncionario() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCpfFuncionario() {
		return cpfFuncionario;
	}

	public Integer getCompetencia() {
		return competencia;
	}

	public Integer getDiaTrabalhados() {
		return diaTrabalhados;
	}

	public void setCpfFuncionario(Long cpfFuncionario) {
		this.cpfFuncionario = cpfFuncionario;
	}

	public void setCompetencia(Integer competencia) {
		this.competencia = competencia;
	}

	public void setDiaTrabalhados(Integer diaTrabalhados) {
		this.diaTrabalhados = diaTrabalhados;
	}

}
