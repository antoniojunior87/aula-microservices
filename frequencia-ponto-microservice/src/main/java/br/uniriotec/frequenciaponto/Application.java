package br.uniriotec.frequenciaponto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {

		// inicia o spring - retorna o contexto
		SpringApplication.run(Application.class, args);
	}

}