package br.uniriotec.frequenciaponto.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.uniriotec.frequenciaponto.entity.FrequenciaFuncionario;

@Repository
@SuppressWarnings("unchecked")
public class GenericDao {

	private EntityManager em;

	@PostConstruct
	public void setUp() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("P_UNIT");
		em = emf.createEntityManager();
	}

	private Session getCurrentSession() {
		return (Session) em.getDelegate();
	}

	public <T> T save(final T o) {

		em.getTransaction().begin();

		T entidade = (T) getCurrentSession().save(o);

		em.getTransaction().commit();

		return entidade;
	}

	public void delete(final Object object) {

		em.getTransaction().begin();

		getCurrentSession().delete(object);

		em.getTransaction().commit();
	}

	public <T> T findOne(final Class<T> type, final Long id) {
		return (T) getCurrentSession().get(type, id);
	}

	public <T> T merge(final T o) {

		em.getTransaction().begin();

		T entidade = (T) getCurrentSession().merge(o);

		em.getTransaction().commit();

		return entidade;
	}

	public <T> void saveOrUpdate(final T o) {

		em.getTransaction().begin();

		getCurrentSession().saveOrUpdate(o);

		em.getTransaction().commit();
	}

	public <T> List<T> findAll(final Class<T> type) {
		final Session session = getCurrentSession();
		final Criteria crit = session.createCriteria(type);
		return crit.list();
	}

	public List<FrequenciaFuncionario> findByCpfFuncionario(Long cpf) {
		final Session session = getCurrentSession();
		final Criteria crit = session.createCriteria(FrequenciaFuncionario.class);

		crit.add(Restrictions.eq("cpfFuncionario", cpf));
		return crit.list();
	}

	public FrequenciaFuncionario findByCpfFuncionarioCompetencia(Long cpf, Integer competencia) {
		final Session session = getCurrentSession();
		final Criteria crit = session.createCriteria(FrequenciaFuncionario.class);

		crit.add(Restrictions.eq("cpfFuncionario", cpf));
		crit.add(Restrictions.eq("competencia", competencia));
		return (FrequenciaFuncionario) crit.uniqueResult();
	}
}
