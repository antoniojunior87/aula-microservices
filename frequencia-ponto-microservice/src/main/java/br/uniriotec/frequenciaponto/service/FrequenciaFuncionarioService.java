package br.uniriotec.frequenciaponto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.uniriotec.frequenciaponto.dao.GenericDao;
import br.uniriotec.frequenciaponto.entity.FrequenciaFuncionario;

@Service
public class FrequenciaFuncionarioService {

	@Autowired
	private GenericDao dao;

	public void salvarTodos(List<FrequenciaFuncionario> lista) {

		for (FrequenciaFuncionario funcionario : lista) {
			dao.save(funcionario);
		}
	}

	public List<FrequenciaFuncionario> listarTodos() {
		return dao.findAll(FrequenciaFuncionario.class);
	}

	public List<FrequenciaFuncionario> obterPorCpfFuncionario(Long cpf) {
		return dao.findByCpfFuncionario(cpf);
	}

	public FrequenciaFuncionario obterPorCpfFuncionarioCompetencia(Long cpf, Integer competencia) {
		return dao.findByCpfFuncionarioCompetencia(cpf, competencia);
	}
}
