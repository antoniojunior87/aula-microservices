package br.uniriotec.frequenciaponto.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.uniriotec.frequenciaponto.entity.FrequenciaFuncionario;

@RestController
public class Servico {

	@Autowired
	private FrequenciaFuncionarioService service;

	@CrossOrigin
	@RequestMapping(path = "/frequenciaponto/{cpf}", method = RequestMethod.GET)
	public List<FrequenciaFuncionario> obterPorCpf(@PathVariable Long cpf) {
		return service.obterPorCpfFuncionario(cpf);
	}

	@CrossOrigin
	@RequestMapping(path = "/frequenciaponto/{cpf}/{competencia}", method = RequestMethod.GET)
	public FrequenciaFuncionario obterPorCpfCompetencia(@PathVariable Long cpf, @PathVariable Integer competencia) {
		return service.obterPorCpfFuncionarioCompetencia(cpf, competencia);
	}

	@PostConstruct
	@CrossOrigin
	@RequestMapping(path = "/setup", method = RequestMethod.GET)
	public String setUp() {

		// insere dados iniciais para teste
		List<FrequenciaFuncionario> lista = new ArrayList<FrequenciaFuncionario>();

		FrequenciaFuncionario f;

		f = new FrequenciaFuncionario();
		f.setId(1L);
		f.setCpfFuncionario(12345678901L);
		f.setCompetencia(201601);
		f.setDiaTrabalhados(20);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(2L);
		f.setCpfFuncionario(12345678901L);
		f.setCompetencia(201602);
		f.setDiaTrabalhados(20);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(3L);
		f.setCpfFuncionario(12345678901L);
		f.setCompetencia(201603);
		f.setDiaTrabalhados(18);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(4L);
		f.setCpfFuncionario(12345678901L);
		f.setCompetencia(201604);
		f.setDiaTrabalhados(23);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(5L);
		f.setCpfFuncionario(20030040099L);
		f.setCompetencia(201601);
		f.setDiaTrabalhados(20);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(6L);
		f.setCpfFuncionario(20030040099L);
		f.setCompetencia(201602);
		f.setDiaTrabalhados(20);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(7L);
		f.setCpfFuncionario(20030040099L);
		f.setCompetencia(201603);
		f.setDiaTrabalhados(18);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(8L);
		f.setCpfFuncionario(20030040099L);
		f.setCompetencia(201604);
		f.setDiaTrabalhados(23);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(9L);
		f.setCpfFuncionario(60070080099L);
		f.setCompetencia(201601);
		f.setDiaTrabalhados(20);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(10L);
		f.setCpfFuncionario(60070080099L);
		f.setCompetencia(201602);
		f.setDiaTrabalhados(20);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(11L);
		f.setCpfFuncionario(60070080099L);
		f.setCompetencia(201603);
		f.setDiaTrabalhados(18);
		lista.add(f);

		f = new FrequenciaFuncionario();
		f.setId(12L);
		f.setCpfFuncionario(60070080099L);
		f.setCompetencia(201604);
		f.setDiaTrabalhados(23);
		lista.add(f);

		try {
			service.salvarTodos(lista);
			return "Dados para teste carregados!";
		} catch (Exception ex) {
			return "Erro - os dados ja estavao carregados?";
		}
	}

}
